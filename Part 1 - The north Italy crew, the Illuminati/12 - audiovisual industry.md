# The audiovisual industry

![CinecittÃ  in Rome](../Images/39746_Cinecitta_1432173962.jpg)

**CinecittÃ ** [[1]](https://en.wikipedia.org/wiki/Cinecitt%C3%A0), Rome, Italy. I've got an old friend of mine that work here, an actress. Her name is **Francesca ** [[2]](https://www.imdb.com/name/nm1840441/?ref_=fn_al_nm_1). A wonderful woman, born in Genoa, class '83. But it's probably just a fortuitous event, or not.

# External Links

1. https://en.wikipedia.org/wiki/Cinecitt%C3%A0
2. https://www.imdb.com/name/nm1840441/?ref_=fn_al_nm_1